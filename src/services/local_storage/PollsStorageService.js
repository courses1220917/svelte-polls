import POLLS from "../../mokups/POLLS";
import LocalStorageService from "./LocalStorageService"

class PollsStorageService extends LocalStorageService {
    static KEY = "polls"; // readonly

    static getStoredPolls() {
        const polls = JSON.parse(this.get(this.KEY));

        if(polls === null) {
            return PollsStorageService.setPrepareContent();
        } else {
            return polls
        }
    }

    static setPolls(polls) {
        this.set(this.KEY, JSON.stringify(polls));
    }

    static setPrepareContent(){
        const content = POLLS;
        PollsStorageService.setPolls(content);
        return PollsStorageService.getStoredPolls();
    }
}

export default PollsStorageService;