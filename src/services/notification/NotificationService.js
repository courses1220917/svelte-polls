import { toasts } from "svelte-toasts";

class NotificationService {

    create = ({title, description, duration, placement, type}) => {
        return toasts.add({
            title,
            description,
            duration,
            placement,
            type,
        });
    }
}

export default NotificationService;