import NotificationService from "./NotificationService";
class PollsNotificationService extends NotificationService {
    duration = 300;

    constructor(props) {
        super();
        this.duration = props?.duration || this.duration;
    }

    createPoll = (question) => {
        let title = "Poll";
        let description = `A poll for "${question}" has been created!`;
        let placement = "bottom-left";
        let type = "success";

        return this.create({
            title,
            description,
            timeout: this.timeout,
            placement,
            type,
        });
    };

    deletePoll = () => {
        let title = "Poll";
        let description = `The poll has been deleted`;
        let placement = "bottom-left";
        let type = "info";

        return this.create({
            title,
            description,
            timeout: this.timeout,
            placement,
            type,
        });
    };
}

export default PollsNotificationService;
