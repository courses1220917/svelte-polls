export const QUESTION_FIELD_LENGTH_ERROR = 'Question must be at least {{MIN_LENGTH}} characters long';
export const ANSWER_A_FIELD_LENGTH_ERROR = 'Answer A must be at least {{MIN_LENGTH}} characters long';
export const ANSWER_B_FIELD_LENGTH_ERROR = 'Answer B must be at least {{MIN_LENGTH}} characters long';