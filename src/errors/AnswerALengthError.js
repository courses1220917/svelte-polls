class AnswerALengthError extends Error {
    constructor(message) {
        super(message);
    }
}

export default AnswerALengthError;
