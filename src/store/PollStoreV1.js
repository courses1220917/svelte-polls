import { writable } from "svelte/store";
import PollsStorageService from "../services/local_storage/PollsStorageService";


const polls = writable(PollsStorageService.getStoredPolls());
const id = Math.random();

export function createPollStore(onChange) {

    function update(updater){
        polls.update((current) => {
            const newVal = updater(current);
            onChange?.(newVal);
            return newVal;
        })
    }

    function updatePolls(poll) {
        polls.update((currentPolls) => {
            return [poll, ...currentPolls]
        });
    }

    function updatePollsVote(option, id){
        polls.update((currentPolls) => {
            let copiedPolls = [...currentPolls];

            let upvotedPoll = copiedPolls.find((poll) => poll.id === id);

            if (option === "a") {
                upvotedPoll.votesA++;
            }
            if (option === "b") {
                upvotedPoll.votesB++;
            }

            return copiedPolls;
        });
    }

    return {
        ...polls,
        updatePolls,
        update
    }
}
