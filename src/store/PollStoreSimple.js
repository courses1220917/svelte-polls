import { writable } from "svelte/store";
import POLLS from "../mokups/POLLS";

const PollStore = writable(POLLS);

export default PollStore;
