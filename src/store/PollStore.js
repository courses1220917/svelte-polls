import { writable } from "svelte/store";
import PollsStorageService from "../services/local_storage/PollsStorageService";

class PollStore {
    static instance = {
        ref: null,
        polls: writable(PollsStorageService.getStoredPolls()),
        id: Math.random(),
    };

    static initializePhase(ctor) {
        return PollStore.instance.ref = new ctor();
    }

    static getInstance(ctor) {
        if(!PollStore.instance.ref) {
            return PollStore.instance.ref = new ctor();
        }
        return PollStore.instance;
    }
    
    static updatePSService(polls) {
        PollsStorageService.setPolls(polls);
        return polls;
    }

    getPolls(){
        return PollStore.instance.polls;
    }

    update(updater){
        PollStore.instance.polls.update((current) => {
            const newVal = updater(current);
            onChange?.(newVal);
            return PollStore.updatePSService(newVal);
        })
    }

    updatePolls(poll) {
        PollStore.instance.polls.update((currentPolls) => {
            return PollStore.updatePSService([poll, ...currentPolls])
        });
    }

    updatePollsVote(option, id){
        PollStore.instance.polls.update((currentPolls) => {
            let copiedPolls = [...currentPolls];

            let upvotedPoll = copiedPolls.find((poll) => poll.id === id);

            if (option === "a") {
                upvotedPoll.votesA++;
            }
            if (option === "b") {
                upvotedPoll.votesB++;
            }

            return PollStore.updatePSService(copiedPolls);
        });
    }

    deletePoll(pollId){
        PollStore.instance.polls.update((currentPolls) => {
            return PollStore.updatePSService(currentPolls.filter((poll) => poll.id !== pollId));
        });
    };
}

export default PollStore;