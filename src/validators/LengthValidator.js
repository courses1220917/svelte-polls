class LengthValidator {
    static message = 'Invalid length';
    static errorKey = null;
    static ErrorType = Error;

    static setErrorMessage(message){
        this.message = message;
        return this;
    }

    static setErrorType(Error, errorKey){
        this.ErrorType = Error;
        this.errorKey = errorKey;
        return this;
    }

    static validate(valueLength, min) {
        
        return new Promise((resolve, reject) => {
            if (valueLength < min) {
                reject({ErrorType: this.ErrorType, message: this.message})
            }
            
            resolve({key: this.errorKey});
        })
    }
}

export default LengthValidator;