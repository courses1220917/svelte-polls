const POLLS = [
    {
        id: 1,
        question: "What is your favourite programming language?",
        answerA: "C#",
        answerB: "Java",
        votesA: 9,
        votesB: 15,
    },
    {
        id: 2,
        question: "What is your favourite framework?",
        answerA: "React",
        answerB: "Svelte",
        votesA: 9,
        votesB: 15,
    },
    {
        id: 3,
        question: "Whats is you favoutite css preprocessor?",
        answerA: "Sass",
        answerB: "Less",
        votesA: 9,
        votesB: 15,
    },
    {
        id: 4,
        question: "What is your favourite code editor?",
        answerA: "VS Code",
        answerB: "Sublime Text",
        votesA: 9,
        votesB: 15,
    }
];

export default POLLS;